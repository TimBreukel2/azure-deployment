# Use a base image with Azure CLI pre-installed
FROM mcr.microsoft.com/azure-cli

# Set the working directory
WORKDIR /workspace

# Add any custom configurations or scripts as needed

# Set the entrypoint command
CMD [ "bash" ]